const mongoose = require("mongoose");

const URI = "mongodb://172.18.0.9:27017/dbmean";
//const URI = "mongodb://root:root@localhost:27017/mean-crud";

mongoose
  .connect(URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
  .then((db) => console.log("db is connected"))
  .catch((err) => console.error("ERROR in connect database:", err));

module.exports = mongoose;
